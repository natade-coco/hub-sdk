export const MainNetwork: ServiceConfig = {
  id: 'did:ethr:0xde17156f366d04552b8c6b74357927a436a878f7',
  type: 'IdentityHubService',
  endpoint: 'https://id.natade-coco.com'
}

export const TestNetwork: ServiceConfig = {
  id: 'did:ethr:0xa0393de11620e4ca4bcaf0cacea671279a1a47ab',
  type: 'IdentityHubService',
  endpoint: 'https://id.natade-coco.net'
}

export enum ServiceType {
  IdentityHubService = 'IdentityHubService',
  MessageHubService = 'MessageHubService',
  AppHubService = 'AppHubService',
  PaymentHubService = 'PaymentHubService'
}

export interface ServiceConfig {
  id: string;
  type: string;
  endpoint: string;
}

export interface Response<T> {
  data: T;
  errors: string[];
}

export interface Resource {
  docs: DIDDocument[];
}

export interface DIDDocument {
  "@context": string;
  id: string;
  authentication: string[];
  publicKey: PublicKey[];
  service: Service[];
}

export interface PublicKey {
  id: string;
  type: string;
  controller: string;
  ethereumAddress: string;
}

export interface Service {
  id: string;
  type: string;
  serviceEndpoint: string;
  dependsOn: string;
}

export interface MessageForm {
  recipient: string;
  alert: string;
  message: string;
  type?: string;
  url?: string;
  did?: string;
}

export interface NotifyPostResult {
  status: string;
  data: string;
}

export interface NotifyResult {
  status: string;
  data: Message;
}

export interface AppSpoke {
  id: string;
  title: string;
  url: string;
  device_id: string;
  fallback_url: string;
}

export interface Message {
  id: string;
  sender: string;
  recipient: string;
  alert: string;
  message: string;
  created_at: number;
  notification_id: string;
  type: string;
  url: string;
}

export interface TxForm {
  amount: number;
  account_id: string;
  redirect_url: string;
}

export interface TxResult {
  transaction: Tx
}

export interface Tx {
  id: string;
  created_at: number;
  amount: number;
  url: string;
  status: string;
  intent_id: string;
  charge_id: string;
  receipt_url: string;
  account: ConnectAccount;
}

export interface ConnectAccount {
  id: string;
  name: string;
  icon_url: string;
  primary_color: string;
  details_submitted: boolean;
  payouts_enabled: boolean;
}
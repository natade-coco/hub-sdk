import dotenv from 'dotenv'
import { SDK, ServiceType } from '../../'

describe('SDK', () => {
  const config = dotenv.config().parsed!;

  const network = {
    type: config.NETWORK_TYPE,
    id: config.NETWORK_ID,
    endpoint: config.NETWORK_ENDPOINT
  };

  const credential = {
    id: config.ID, 
    secret: config.SECRET
  }

  const jwt = config.TEST_JWT
  const connect_account = config.CONNECT_ACCOUNT

  var client: SDK;
  
  beforeAll(async () => {
    const test = true
    client = await SDK.init({
      test, // override
      ...credential
    });
  })
  
  test('Configs (SUCCESS)', async () => {
    const result = client.Configs().find(e => e.type === 'IdentityHubService')!;
    expect(result).toEqual(network);
  });

  test('Resolve (SUCCESS)', async () => {
    const result = await client.Resolve(network.id);
    expect(result.id).toEqual(network.id);
  });

  test('Notify with did (SUCCESS)', async () => {
    const form = {
      recipient: credential.id,
      alert: 'APP_TITLE',
      message: 'notification with did url',
      did: network.id
    }
    const result = await client.Notify(form);
    expect(result.data.url).toEqual(network.endpoint + '/docs/' + network.id);
  });

  test('Notify with url (SUCCESS)', async () => {
    const form = {
      recipient: credential.id,
      alert: 'APP_TITLE',
      message: 'notification with custom url',
      url: network.endpoint
    }
    const result = await client.Notify(form);
    expect(result.data.url).toEqual(network.endpoint);
  });

  test('Notify without link (SUCCESS)', async () => {
    const form = {
      recipient: credential.id,
      alert: 'APP_TITLE',
      message: 'notification with no link'
    }
    const result = await client.Notify(form);
    expect(result.data.url).toEqual('');
  });

  test('Notify with url and type (SUCCESS)', async () => {
    const form = {
      recipient: credential.id,
      alert: 'APP_TITLE',
      type: 'SPOT_INFO_RECEIVED',
      message: 'notification with custom url',
      url: network.endpoint
    }
    const result = await client.Notify(form);
    expect(result.data.url).toEqual(network.endpoint);
  });

  test('VerifyJWT', async () => {
    const result = await client.VerifyJWT(jwt, ServiceType.AppHubService)
    expect(result.signer.type).toEqual('EcdsaPublicKeySecp256k1');
  });
});

describe('SDK with JWT', () => {
  const config = dotenv.config().parsed!;

  const network = {
    type: config.NETWORK_TYPE,
    id: config.NETWORK_ID,
    endpoint: config.NETWORK_ENDPOINT
  };

  const identityHubToken = config.IDENTITYHUB_JWT
  const paymentHubToken = config.PAYMENTHUB_JWT
  const connect_account = config.CONNECT_ACCOUNT

  var client: SDK;
  
  beforeAll(async () => {
    const test = true
    client = await SDK.initWithJWT({
      test, // override
      jwt: identityHubToken
    });
  })

  test('NewPaymentTx -> GetPaymentTx -> DelPaymentTx (SUCCESS)', async () => {
    const form = {
      amount: 100,
      account_id: connect_account,
      redirect_url: "http://localhost:3030",
    }
    const r1 = await client.NewPaymentTx(paymentHubToken, form);
    const r2 = await client.GetPaymentTx(paymentHubToken, r1.transaction.id);
    const r3 = await client.DelPaymentTx(paymentHubToken, r2.transaction.id);
    expect(r1.transaction.id).toEqual(r3.transaction.id);
  });
});
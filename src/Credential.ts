import { ServiceConfig } from './Type';
const didJwt = require('did-jwt');

export interface Credential {
  id: string;
  secret: string;
}

export interface ISigner {
  JWT(cfg: ServiceConfig): Promise<string>;
}

export class Signer implements ISigner {
  constructor(private credential: Credential) { }

  public JWT(cfg: ServiceConfig): Promise<string> {
    return new Promise((resolve, reject) => {
      const issuer = this.credential.id;
      const signer = didJwt.SimpleSigner(this.credential.secret);
      const payload = { 'aud': cfg.id }
      didJwt.createJWT(payload, { 'issuer': issuer, 'signer': signer, 'alg': 'ES256K-R', 'expiresIn': 60 })
        .then((jwt: string) => resolve(jwt))
        .catch((error: any) => reject(error));
    });
  }
}

export class StaticToken implements ISigner {
  constructor(private jwt: string) { }

  public JWT(cfg: ServiceConfig): Promise<string> {
    return new Promise((resolve, reject) => {
      resolve(this.jwt)
    })
  }
}
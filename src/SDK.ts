import { Resolver } from 'did-resolver'
import didJWT from 'did-jwt'
import { ISigner, Signer, StaticToken } from './Credential';
import * as identityHub from './services/IdentityHub';
import * as messageHub from './services/MessageHub';
import * as paymentHub from './services/PaymentHub';
import * as appHub from './services/AppHub';
import {
  MainNetwork,
  TestNetwork,
  ServiceConfig,
  ServiceType,
  DIDDocument,
  MessageForm,
  NotifyResult,
  TxForm,
  TxResult,
  AppSpoke,
} from './Type';

export interface InitOpts {
  id: string,
  secret: string,
  test?: boolean,
}

export interface InitWithJWTOpts {
  jwt: string,
  test?: boolean,
}

export class SDK {
  private constructor(private signer: ISigner, private configs: ServiceConfig[]) { }

  public static init = async (opts: InitOpts) => {
    const signer = new Signer({ id: opts.id, secret: opts.secret });
    const identities = await identityHub.GetIdentities(opts.test ? TestNetwork : MainNetwork, signer);
    const configs: ServiceConfig[] = identities.filter(e => e.service !== undefined).map((i) => {
      return { id: i.id, type: i.service[0].type, endpoint: i.service[0].serviceEndpoint }
    });
    return new SDK(signer, configs);
  }

  public static initWithJWT = async (opts: InitWithJWTOpts) => {
    const signer = new StaticToken(opts.jwt)
    const identities = await identityHub.GetIdentities(opts.test ? TestNetwork : MainNetwork, signer);
    const configs: ServiceConfig[] = identities.filter(e => e.service !== undefined).map((i) => {
      return { id: i.id, type: i.service[0].type, endpoint: i.service[0].serviceEndpoint }
    });
    return new SDK(signer, configs);
  }

  public Configs(): ServiceConfig[] {
    return this.configs;
  }

  public Resolve(id: string): Promise<DIDDocument> {
    const config = this.configs.find(e => e.type === ServiceType.IdentityHubService)!;
    return identityHub.GetIdentity(config, this.signer, id);
  }

  public Notify(form: MessageForm): Promise<NotifyResult> {
    const config = this.configs.find(e => e.type === ServiceType.MessageHubService)!;
    return new Promise((resolve, reject) => {
      messageHub.PostMessage(config, this.signer, form)
        .then(result => {
          return messageHub.GetMessage(config, this.signer, result.data);
        })
        .then(resolve)
        .catch(reject)
    });
  }

  public VerifyJWT(jwt: string, audience: ServiceType): Promise<any> {
    const config = this.configs.find(e => e.type === ServiceType.IdentityHubService)!;
    const audienceConfig = this.configs.find(e => e.type === audience)!;
    const resolver = new Resolver(identityHub.getResolver(config))
    return didJWT.verifyJWT(jwt, { resolver: resolver, audience: audienceConfig.id })
  }

  public NewPaymentTx(token: string, form: TxForm): Promise<TxResult> {
    const config = this.configs.find(e => e.type === ServiceType.PaymentHubService)!;
    const signer = new StaticToken(token)
    return paymentHub.PostTx(config, signer, form);
  }

  public GetPaymentTx(token: string, id: string): Promise<TxResult> {
    const config = this.configs.find(e => e.type === ServiceType.PaymentHubService)!;
    const signer = new StaticToken(token)
    return paymentHub.GetTx(config, signer, id);
  }

  public DelPaymentTx(token: string, id: string): Promise<TxResult> {
    const config = this.configs.find(e => e.type === ServiceType.PaymentHubService)!;
    const signer = new StaticToken(token)
    return paymentHub.DelTx(config, signer, id);
  }

  public AppSpoke(id: string): Promise<AppSpoke> {
    const config = this.configs.find(e => e.type === ServiceType.AppHubService)!;
    return new Promise((resolve, reject) => {
      appHub.GetSpoke(config, this.signer, id)
        .then(resolve)
        .catch(reject)
    });
  }
}

import axios, { AxiosRequestConfig } from 'axios';
import { Resolver, ParsedDID, DIDDocument } from 'did-resolver'
import { ISigner } from '../Credential';
import { ServiceConfig, Response, Resource, DIDDocument as NCDIDDocument } from '../Type';

export function getResolver(config: ServiceConfig) {
  async function resolve(did: string, parsed: ParsedDID, didResolver: Resolver): Promise<DIDDocument | null> {
    const cfg: AxiosRequestConfig = {
      baseURL: config.endpoint,
      method: 'GET',
      url: '/docs/' + did,
      validateStatus: (status) => status >= 200 && status < 300
    }
    const res = await axios.request<DIDDocument>(cfg)
    const didDoc = res.data
  
    // https://github.com/w3c-ccg/ld-cryptosuite-registry/issues/8
    didDoc.publicKey = didDoc.publicKey.map((e) => { e.type = 'EcdsaPublicKeySecp256k1'; return e; } )
  
    return didDoc
  }
  return { ethr: resolve }
}

export function GetIdentities(config: ServiceConfig, signer: ISigner, params?: any): Promise<NCDIDDocument[]> {
  return new Promise(async (resolve, reject) => {
    const jwt = await signer.JWT(config).catch(reject);
    const cfg: AxiosRequestConfig = {
      headers: { Authorization: 'Bearer ' + jwt },
      baseURL: config.endpoint,
      method: 'GET',
      url: '/docs',
      params: params,
      validateStatus: (status) => status >= 200 && status < 300
    }
    axios.request<Response<Resource>>(cfg)
      .then((response) => { resolve(response.data.data.docs) })
      .catch((error: any) => reject(error));
  });
}

export function GetIdentity(config: ServiceConfig, signer: ISigner, id: string): Promise<NCDIDDocument> {
  return new Promise(async (resolve, reject) => {
    const jwt = await signer.JWT(config).catch(reject);
    const cfg: AxiosRequestConfig = {
      headers: { Authorization: 'Bearer ' + jwt },
      baseURL: config.endpoint,
      method: 'GET',
      url: '/docs/' + id,
      validateStatus: (status) => status >= 200 && status < 300
    }
    axios.request<NCDIDDocument>(cfg)
      .then((response) => { resolve(response.data) })
      .catch((error: any) => reject(error));
  });
}
import axios, { AxiosRequestConfig } from 'axios';
import { ISigner } from '../Credential';
import { ServiceConfig, MessageForm, NotifyPostResult, NotifyResult } from '../Type';

export function PostMessage(config: ServiceConfig, signer: ISigner, form: MessageForm): Promise<NotifyPostResult> {
  return new Promise(async (resolve, reject) => {
    const jwt = await signer.JWT(config).catch(reject);
    const cfg: AxiosRequestConfig = {
      headers: { Authorization: 'Bearer ' + jwt },
      baseURL: config.endpoint,
      method: 'POST',
      url: '/message',
      data: form,
      validateStatus: (status) => status >= 200 && status < 300
    }
    axios.request<NotifyPostResult>(cfg)
      .then((response) => { resolve(response.data) })
      .catch((error: any) => reject(error));
  });
}

export function GetMessage(config: ServiceConfig, signer: ISigner, id: string): Promise<NotifyResult> {
  return new Promise(async (resolve, reject) => {
    const jwt = await signer.JWT(config).catch(reject);
    const cfg: AxiosRequestConfig = {
      headers: { Authorization: 'Bearer ' + jwt },
      baseURL: config.endpoint,
      method: 'GET',
      url: '/message/' + id,
      validateStatus: (status) => status >= 200 && status < 300
    }
    axios.request<NotifyResult>(cfg)
      .then((response) => { resolve(response.data) })
      .catch((error: any) => reject(error));
  });
}
import axios, { AxiosRequestConfig } from 'axios';
import { ISigner } from '../Credential';
import { ServiceConfig, TxForm, TxResult, Response } from '../Type';

export function PostTx(config: ServiceConfig, signer: ISigner, form: TxForm): Promise<TxResult> {
  return new Promise(async (resolve, reject) => {
    const jwt = await signer.JWT(config).catch(reject);
    const cfg: AxiosRequestConfig = {
      headers: { Authorization: 'Bearer ' + jwt },
      baseURL: config.endpoint,
      method: 'POST',
      url: '/tx',
      data: form,
      validateStatus: (status) => status >= 200 && status < 300
    }
    axios.request<Response<TxResult>>(cfg)
      .then((response) => { resolve(response.data.data) })
      .catch((error: any) => reject(error));
  });
}

export function GetTx(config: ServiceConfig, signer: ISigner, id: string): Promise<TxResult> {
  return new Promise(async (resolve, reject) => {
    const jwt = await signer.JWT(config).catch(reject);
    const cfg: AxiosRequestConfig = {
      headers: { Authorization: 'Bearer ' + jwt },
      baseURL: config.endpoint,
      method: 'GET',
      url: '/tx/' + id,
      validateStatus: (status) => status >= 200 && status < 300
    }
    axios.request<Response<TxResult>>(cfg)
      .then((response) => { resolve(response.data.data) })
      .catch((error: any) => reject(error));
  });
}

export function DelTx(config: ServiceConfig, signer: ISigner, id: string): Promise<TxResult> {
  return new Promise(async (resolve, reject) => {
    const jwt = await signer.JWT(config).catch(reject);
    const cfg: AxiosRequestConfig = {
      headers: { Authorization: 'Bearer ' + jwt },
      baseURL: config.endpoint,
      method: 'DELETE',
      url: '/tx/' + id,
      validateStatus: (status) => status >= 200 && status < 300
    }
    axios.request<Response<TxResult>>(cfg)
      .then((response) => { resolve(response.data.data) })
      .catch((error: any) => reject(error));
  });
}
import axios, { AxiosRequestConfig } from 'axios';
import { ISigner } from '../Credential';
import { ServiceConfig, AppSpoke } from '../Type';

export function GetSpoke(config: ServiceConfig, signer: ISigner, id: string): Promise<AppSpoke> {
  return new Promise(async (resolve, reject) => {
    const jwt = await signer.JWT(config).catch(reject);
    const cfg: AxiosRequestConfig = {
      headers: { Authorization: 'Bearer ' + jwt },
      baseURL: config.endpoint,
      method: 'GET',
      url: '/apps/' + id + '/spoke',
      validateStatus: (status) => status >= 200 && status < 300
    }
    axios.request<AppSpoke>(cfg)
      .then((response) => { resolve(response.data) })
      .catch((error: any) => reject(error));
  });
}
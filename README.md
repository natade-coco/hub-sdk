# @natade-coco/hub-sdk

> Library for accessing data of natadeCOCO Hub services from natadeCOCO content app.

## 🚧 Release Candidate

This is _pre-release_ software. Changes may occur at any time, and
certain features might be missing or broken. 

## 📌 Requirements

You have to activate a natadeCOCO Unit via [natadeCOCO Console](https://console.natade-coco.com/) and register a content app.

## ⚙️ Install

```bash
npm install --save @natade-coco/hub-sdk
```

## 🚀 Usage

```js
import { SDK } from '@natade-coco/hub-sdk';

const client = await SDK.init({
  test: true, // don't use in production
  id: 'CONTENT_APP_DID',
  secret: 'CONTENT_APP_SECRET'
});
```

or

```js
import { SDK } from '@natade-coco/hub-sdk';

const client = await SDK.initWithJWT({
  test: true, // don't use in production
  jwt: 'VIA_POCKET_SDK'
});
```

## 📝 API

### `Configs(): ServiceConfig[]`

Get service configurations.

#### `ServiceConfig`

```
{
  id: 'did:ethr:0x1234...',
  type: 'IdentityHubService',
  endpoint: 'https://id.natade-coco.com'
}
```

### `Resolve(id: string): Promise<DIDDocument>`

Resolve DIDDocument from DID.

| param | type     | example              |
| :---- | :------- | :------------------- |
| `id`  | `string` | 'did:ethr:0x1234...' |

#### `DIDDocument`

```
{
  '@context': 'https://w3id.org/did/v1',
  id: 'did:ethr:0x1234...',
  authentication: [ 'did:ethr:0x1234...#keys-1' ],
  publicKey: [
    {
      id: 'did:ethr:0x1234...#keys-1',
      type: 'EdDsaSAPublicKeySecp256k1',
      controller: 'did:ethr:0x1234...',
      ethereumAddress: '0x1234...'
    }
  ],
  service: [
    {
      id: 'did:ethr:0x1234...;svc-1',
      type: 'IdentityHubService',
      serviceEndpoint: 'https://id.natade-coco.com'
    }
  ]
}
```

### `Notify(form: MessageForm): Promise<NotifyResult>`

Send a PushNotification to recipient from content app.

#### `MessageForm`

| param       | type      | example                   |
| :---------- | :-------- | :------------------------ |
| `recipient` | `string`  | 'did:ethr:0x1234...'      |
| `alert`     | `string`  | 'YOUR_APP_TITLE'          |
| `message`   | `string`  | 'MESSAGE_BODY'            |
| `type`      | `?string`  | 'INFO_RECEIVED' or 'SPOT_INFO_RECEIVED' |
| `url`       | `?string` | 'https://natade-coco.com' |
| `did`       | `?string` | 'did:ethr:0x1234...'      |

#### `NotifyResult`

```
{
  status: 'success',
  data: {
    id: '...',
    sender: 'did:ethr:0x1234...',
    recipient: 'did:ethr:0x1234...',
    alert: 'APP_TITLE',
    message: 'MESSAGE_BODY',
    created_at: 1587956354,
    notification_id: '...',
    type: 'INFO_RECEIVED',
    url: 'https://id.natade-coco.com/docs/did:ethr:0x1234...'
  }
}
```

### `VerifyJWT(jwt: string, audience: ServiceType): Promise<any>`

Verify a jwt by did-jwt using natadeCOCO network.

### `NewPaymentTx(token: string, form: TxForm): Promise<TxResult>`

Create a new Payment Transaction

#### `TxForm`

| param          | type     | example               |
| :------------- | :------- | :-------------------- |
| `amount`       | `number` | 100                   |
| `account_id`   | `string` | 'ACCOUNT_ID'     |
| `redirect_url` | `string` | 'https://example.com' |

#### `TxResult`

```
{
  transaction: {
    id: '...',',
    created_at: 1597902108,
    amount: 100,
    url: '...',
    status: 'created',
    account: {
      id: 'acct_XXXXXXXX',
      name: 'TEST_SHOP',
      icon_url: '...',
      primary_color: '#000000',
      details_submitted: true,
      payouts_enabled: true
    }
  }
}
```

### `GetPaymentTx(token: string, id: string): Promise<TxResult>`

Get a Payment Transaction

### `DelPaymentTx(token: string, id: string): Promise<TxResult>`

Delete a Payment Transaction

### `AppSpoke(id: string): Promise<AppSpoke>`

Get AppSpoke.

#### `AppSpoke`

```
{
  id: 'string',
  title: 'string',
  url: 'string',
  device_id: 'string',
  fallback_url: 'string'
}
```
